   <?php $this->load->view('incl/head'); ?>

   <!-- CSS Internal -->
   <style>
      #map {
        height: 560px;
        width: 100%;
      }

      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>

    <body class="widescreen fixed-left-void">

        <!-- Begin page -->
        <div id="wrapper" class="forced enlarged">

            <!-- Top Bar Start -->
            <?php $this->load->view('incl/topbar'); ?>
            <!-- Top Bar End -->
            
            <!-- Side Bar Start -->
            <?php $this->load->view('incl/sidebar'); ?>
            <!-- Side Bar End -->

            <div class="content-page">
                <!-- Start content [Tempat menulis code utama dsini] -->
                <div class="content" style="padding:0 !important;">
                   <div id="map"></div>
                </div> <!-- content -->

                <!-- Footer Start -->
                <?php  $this->load->view('incl/footer'); ?>
                <!-- Footer End -->
            </div>
            
        </div>
        <!-- jQuery  & Javacript -->
        <?php $this->load->view('incl/script'); ?>
    
        <!-- Javascript Internal -->
        <script type="text/javascript">

            $(document).ready(function () {
                showMap();     
            });

            function showMap() {

                var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(-8.557241, 125.576607),
                mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                var infowindow = new google.maps.InfoWindow();

                var marker, i;
                var image = '<?= base_url();?>tower.png';

                $.ajax({
                    type: "GET",
                    url: "<?=base_url('')?>api/bts",
                    dataType: "JSON",
                    success: function (response) {
                        var r = response['data'];
                        for (i = 0; i < r.length; i++) {  
                            marker = new google.maps.Marker({
                            position: new google.maps.LatLng(r[i].lat, r[i].lng),
                            map: map,
                            icon: image
                        });

                        google.maps.event.addListener(marker, 'click', (function(marker, i) {
                            return function() {
                            infowindow.setContent('<b>BTS </b>'+r[i].nama_site);
                            infowindow.open(map, marker);

                            $('.nmaBts').html(r[i].nama_site);
                            $('#exampleModal').modal('show');
                            
                            }
                        })(marker, i));

                        }
                    }
                });

                // for (i = 0; i < locations.length; i++) {  
                // marker = new google.maps.Marker({
                //     position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                //     map: map,
                //     icon: image
                // });

                // google.maps.event.addListener(marker, 'click', (function(marker, i) {
                //     return function() {
                //     infowindow.setContent(locations[i][0]);
                //     infowindow.open(map, marker);
                //     }
                // })(marker, i));
                // }
            }
        </script>

       <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">BTS <span class="nmaBts"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
                </li>
                </ul>
                <div class="tab-content" id="myTabContent" style="padding:0px;">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <table class="table">
                                <thead class="thead-light">
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">First</th>
                                    <th scope="col">Last</th>
                                    <th scope="col">Handle</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <th scope="row">1</th>
                                    <td>Mark</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                    </tr>
                                    <tr>
                                    <th scope="row">2</th>
                                    <td>Jacob</td>
                                    <td>Thornton</td>
                                    <td>@fat</td>
                                    </tr>
                                    <tr>
                                    <th scope="row">3</th>
                                    <td>Larry</td>
                                    <td>the Bird</td>
                                    <td>@twitter</td>
                                    </tr>
                                </tbody>
                            </table>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
                </div>  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Send message</button>
            </div>
            </div>
        </div>
        </div>
 
    </body>
</html>