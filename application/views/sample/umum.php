<!-- Memanggil "views/incl/head.php" -->
<?php $this->load->view('incl/head'); ?>

<!-- CSS Internal -->
 <body class="widescreen fixed-left-void">

     <!-- Begin page -->
     <div id="wrapper" class="forced enlarged">

         <!-- Top Bar Start -->
         <?php $this->load->view('incl/topbar'); ?>
         <!-- Top Bar End -->
         
         <!-- Side Bar Start -->
         <?php $this->load->view('incl/sidebar'); ?>
         <!-- Side Bar End -->

         <div class="content-page">
             <!-- Start content [Tempat menulis code utama dsini] -->
             <div class="content" style="padding:0 !important;">
                Hello 
             </div> <!-- content -->

             <!-- Footer Start -->
             <?php  $this->load->view('incl/footer'); ?>
             <!-- Footer End -->
         </div>
         
     </div>

     <!-- jQuery  & Javacript -->
     <?php $this->load->view('incl/script'); ?>
 
     <!-- Javascript Internal -->
 
 </body>
</html>