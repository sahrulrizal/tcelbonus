<!-- Memanggil "views/incl/head.php" -->
<?php $this->load->view('incl/head'); ?>

<!-- CSS Internal -->
 <body class="widescreen fixed-left-void">

     <!-- Begin page -->
     <div id="wrapper" class="forced enlarged">

         <!-- Top Bar Start -->
         <?php $this->load->view('incl/topbar'); ?>
         <!-- Top Bar End -->
         
         <!-- Side Bar Start -->
         <?php $this->load->view('incl/sidebar'); ?>
         <!-- Side Bar End -->

         <div class="content-page">
             <!-- Start content [Tempat menulis code utama dsini] -->
             <div class="content">
                    <div class="container-fluid">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="btn-group pull-right m-t-15">
                                    <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings</button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                                        <a class="dropdown-item" href="#">Dropdown One</a>
                                        <a class="dropdown-item" href="#">Dropdown Two</a>
                                        <a class="dropdown-item" href="#">Dropdown Three</a>
                                        <a class="dropdown-item" href="#">Dropdown Four</a>
                                    </div>
                                </div>

                                <h4 class="page-title">Dashboard</h4>
                                <p class="text-muted page-title-alt">Welcome to Ubold admin panel !</p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-xl-3">
                                <div class="widget-bg-color-icon card-box">
                                    <div class="bg-icon bg-info pull-left">
                                        <i class="md md-attach-money text-white"></i>
                                    </div>
                                    <div class="text-right">
                                        <h3 class="text-dark"><span class="counter">31,570</span></h3>
                                        <p class="text-muted mb-0">Total Revenue</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-6 col-xl-3">
                                <div class="widget-bg-color-icon card-box">
                                    <div class="bg-icon bg-pink pull-left">
                                        <i class="md md-add-shopping-cart text-white"></i>
                                    </div>
                                    <div class="text-right">
                                        <h3 class="text-dark"><span class="counter">280</span></h3>
                                        <p class="text-muted mb-0">Today's Sales</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-6 col-xl-3">
                                <div class="widget-bg-color-icon card-box">
                                    <div class="bg-icon bg-purple pull-left">
                                        <i class="md md-equalizer text-white"></i>
                                    </div>
                                    <div class="text-right">
                                        <h3 class="text-dark"><span class="counter">0.16</span>%</h3>
                                        <p class="text-muted mb-0">Conversion</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-6 col-xl-3">
                                <div class="widget-bg-color-icon card-box">
                                    <div class="bg-icon bg-success pull-left">
                                        <i class="md md-remove-red-eye text-white"></i>
                                    </div>
                                    <div class="text-right">
                                        <h3 class="text-dark"><span class="counter">64,570</span></h3>
                                        <p class="text-muted mb-0">Today's Visits</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-lg-12 col-xl-4">
                                <div class="card-box">
                                    <h4 class="text-dark header-title m-t-0 m-b-30">Total Revenue</h4>

                                    <div class="widget-chart text-center">
                                        <div style="display:inline;width:150px;height:150px;"><canvas width="150" height="150"></canvas><input class="knob" data-width="150" data-height="150" data-linecap="round" data-fgcolor="#566676" value="80" data-skin="tron" data-angleoffset="180" data-readonly="true" data-thickness=".15" readonly="readonly" style="width: 79px; height: 50px; position: absolute; vertical-align: middle; margin-top: 50px; margin-left: -114px; border: 0px; background: none; font: bold 30px Arial; text-align: center; color: rgb(86, 102, 118); padding: 0px; -webkit-appearance: none;"></div>
                                        <h5 class="text-muted m-t-20">Total sales made today</h5>
                                        <h2 class="font-600">$75</h2>
                                        <ul class="list-inline m-t-15">
                                            <li class="list-inline-item">
                                                <h5 class="text-muted m-t-20">Target</h5>
                                                <h4 class="m-b-0">$1000</h4>
                                            </li>
                                            <li class="list-inline-item">
                                                <h5 class="text-muted m-t-20">Last week</h5>
                                                <h4 class="m-b-0">$523</h4>
                                            </li>
                                            <li class="list-inline-item">
                                                <h5 class="text-muted m-t-20">Last Month</h5>
                                                <h4 class="m-b-0">$965</h4>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-12 col-xl-8">
                                <div class="card-box">
                                    <h4 class="text-dark header-title m-t-0">Sales Analytics</h4>
                                    <div class="text-center">
                                        <ul class="list-inline chart-detail-list">
                                            <li class="list-inline-item">
                                                <h5><i class="fa fa-circle m-r-5" style="color: #2bbbad;"></i>Desktops</h5>
                                            </li>
                                            <li class="list-inline-item">
                                                <h5><i class="fa fa-circle m-r-5" style="color: #5d9cec;"></i>Tablets</h5>
                                            </li>
                                            <li class="list-inline-item">
                                                <h5><i class="fa fa-circle m-r-5" style="color: #dcdcdc;"></i>Mobiles</h5>
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="morris-bar-stacked" style="height: 310px; position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg height="310" version="1.1" width="610" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.2</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><text x="32.515625" y="271" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan></text><path fill="none" stroke="#eeeeee" d="M45.015625,271.5H585" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.515625" y="209.5" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">100</tspan></text><path fill="none" stroke="#eeeeee" d="M45.015625,209.5H585" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.515625" y="148" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">200</tspan></text><path fill="none" stroke="#eeeeee" d="M45.015625,148.5H585" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.515625" y="86.5" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">300</tspan></text><path fill="none" stroke="#eeeeee" d="M45.015625,86.5H585" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.515625" y="25" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">400</tspan></text><path fill="none" stroke="#eeeeee" d="M45.015625,25.5H585" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="560.4552556818181" y="283.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2015</tspan></text><text x="462.27627840909093" y="283.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013</tspan></text><text x="364.0973011363636" y="283.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2011</tspan></text><text x="265.9183238636364" y="283.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2009</tspan></text><text x="167.7393465909091" y="283.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2007</tspan></text><text x="69.56036931818181" y="283.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2005</tspan></text><rect x="51.15181107954545" y="243.325" width="36.81711647727273" height="27.67500000000001" rx="0" ry="0" fill="#5fbeaa" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="51.15181107954545" y="132.625" width="36.81711647727273" height="110.69999999999999" rx="0" ry="0" fill="#5d9cec" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="51.15181107954545" y="71.125" width="36.81711647727273" height="61.5" rx="0" ry="0" fill="#ebeff2" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="100.24129971590908" y="224.875" width="36.81711647727273" height="46.125" rx="0" ry="0" fill="#5fbeaa" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="100.24129971590908" y="184.9" width="36.81711647727273" height="39.974999999999994" rx="0" ry="0" fill="#5d9cec" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="100.24129971590908" y="135.70000000000002" width="36.81711647727273" height="49.19999999999999" rx="0" ry="0" fill="#ebeff2" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="149.3307883522727" y="209.5" width="36.81711647727273" height="61.5" rx="0" ry="0" fill="#5fbeaa" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="149.3307883522727" y="154.15" width="36.81711647727273" height="55.349999999999994" rx="0" ry="0" fill="#5d9cec" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="149.3307883522727" y="119.71000000000001" width="36.81711647727273" height="34.44" rx="0" ry="0" fill="#ebeff2" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="198.42027698863637" y="224.875" width="36.81711647727273" height="46.125" rx="0" ry="0" fill="#5fbeaa" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="198.42027698863637" y="184.9" width="36.81711647727273" height="39.974999999999994" rx="0" ry="0" fill="#5d9cec" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="198.42027698863637" y="130.165" width="36.81711647727273" height="54.735000000000014" rx="0" ry="0" fill="#ebeff2" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="247.509765625" y="209.5" width="36.81711647727273" height="61.5" rx="0" ry="0" fill="#5fbeaa" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="247.509765625" y="154.15" width="36.81711647727273" height="55.349999999999994" rx="0" ry="0" fill="#5d9cec" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="247.509765625" y="80.35" width="36.81711647727273" height="73.80000000000001" rx="0" ry="0" fill="#ebeff2" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="296.59925426136357" y="224.875" width="36.81711647727273" height="46.125" rx="0" ry="0" fill="#5fbeaa" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="296.59925426136357" y="184.9" width="36.81711647727273" height="39.974999999999994" rx="0" ry="0" fill="#5d9cec" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="296.59925426136357" y="117.25" width="36.81711647727273" height="67.65" rx="0" ry="0" fill="#ebeff2" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="345.68874289772725" y="240.25" width="36.81711647727273" height="30.75" rx="0" ry="0" fill="#5fbeaa" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="345.68874289772725" y="215.65" width="36.81711647727273" height="24.599999999999994" rx="0" ry="0" fill="#5d9cec" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="345.68874289772725" y="163.375" width="36.81711647727273" height="52.275000000000006" rx="0" ry="0" fill="#ebeff2" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="394.7782315340909" y="224.875" width="36.81711647727273" height="46.125" rx="0" ry="0" fill="#5fbeaa" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="394.7782315340909" y="184.9" width="36.81711647727273" height="39.974999999999994" rx="0" ry="0" fill="#5d9cec" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="394.7782315340909" y="152.92000000000002" width="36.81711647727273" height="31.97999999999999" rx="0" ry="0" fill="#ebeff2" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="443.8677201704545" y="240.25" width="36.81711647727273" height="30.75" rx="0" ry="0" fill="#5fbeaa" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="443.8677201704545" y="215.65" width="36.81711647727273" height="24.599999999999994" rx="0" ry="0" fill="#5d9cec" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="443.8677201704545" y="168.29500000000002" width="36.81711647727273" height="47.35499999999999" rx="0" ry="0" fill="#ebeff2" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="492.95720880681813" y="224.875" width="36.81711647727273" height="46.125" rx="0" ry="0" fill="#5fbeaa" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="492.95720880681813" y="184.9" width="36.81711647727273" height="39.974999999999994" rx="0" ry="0" fill="#5d9cec" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="492.95720880681813" y="129.55" width="36.81711647727273" height="55.349999999999994" rx="0" ry="0" fill="#ebeff2" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="542.0466974431818" y="209.5" width="36.81711647727273" height="61.5" rx="0" ry="0" fill="#5fbeaa" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="542.0466974431818" y="154.15" width="36.81711647727273" height="55.349999999999994" rx="0" ry="0" fill="#5d9cec" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="542.0466974431818" y="74.20000000000002" width="36.81711647727273" height="79.94999999999999" rx="0" ry="0" fill="#ebeff2" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect></svg><div class="morris-hover morris-default-style" style="display: none;"></div></div>
                                </div>
                            </div>



                        </div>
                        <!-- end row -->


                        <div class="row">

                            <div class="col-lg-12 col-xl-6">
                                <div class="card-box">
                                    <h4 class="text-dark header-title m-t-0">Total Sales</h4>

                                    <div class="text-center">
                                        <ul class="list-inline chart-detail-list">
                                            <li class="list-inline-item">
                                                <h5><i class="fa fa-circle m-r-5" style="color: #2bbbad;"></i>Desktops</h5>
                                            </li>
                                            <li class="list-inline-item">
                                                <h5><i class="fa fa-circle m-r-5" style="color: #5d9cec;"></i>Tablets</h5>
                                            </li>
                                            <li class="list-inline-item">
                                                <h5><i class="fa fa-circle m-r-5" style="color: #ebeff2;"></i>Mobiles</h5>
                                            </li>
                                        </ul>
                                    </div>

                                    <div id="morris-area-with-dotted" style="height: 353px; position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg height="353" version="1.1" width="440" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.2</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><text x="32.515625" y="314" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan></text><path fill="none" stroke="#eef0f2" d="M45.015625,314.5H415" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.515625" y="241.75" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">50</tspan></text><path fill="none" stroke="#eef0f2" d="M45.015625,241.5H415" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.515625" y="169.5" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">100</tspan></text><path fill="none" stroke="#eef0f2" d="M45.015625,169.5H415" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.515625" y="97.25" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">150</tspan></text><path fill="none" stroke="#eef0f2" d="M45.015625,97.5H415" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.515625" y="25" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">200</tspan></text><path fill="none" stroke="#eef0f2" d="M45.015625,25.5H415" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="415" y="326.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2015</tspan></text><text x="353.3640817549064" y="326.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2014</tspan></text><text x="291.7281635098128" y="326.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013</tspan></text><text x="229.9233797352807" y="326.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2012</tspan></text><text x="168.28746149018713" y="326.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2011</tspan></text><text x="106.65154324509356" y="326.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2010</tspan></text><text x="45.015625" y="326.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2009</tspan></text><path fill="#b0caea" stroke="none" d="M45.015625,227.3C60.42460456127339,187.5625,91.24256368382018,79.1875,106.65154324509356,68.35C122.06052280636695,57.51249999999999,152.87848192891374,140.6,168.28746149018713,140.6C183.6964410514605,140.6,214.5144001740073,68.35,229.9233797352807,68.35C245.37457567891371,68.35,276.2769675661798,140.6,291.7281635098128,140.6C307.1371430710862,140.6,337.955102193633,79.1875,353.3640817549064,68.35C368.7730613161798,57.51249999999999,399.5910204387266,57.51249999999998,415,53.89999999999998L415,314L45.015625,314Z" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></path><path fill="none" stroke="#5d9cec" d="M45.015625,227.3C60.42460456127339,187.5625,91.24256368382018,79.1875,106.65154324509356,68.35C122.06052280636695,57.51249999999999,152.87848192891374,140.6,168.28746149018713,140.6C183.6964410514605,140.6,214.5144001740073,68.35,229.9233797352807,68.35C245.37457567891371,68.35,276.2769675661798,140.6,291.7281635098128,140.6C307.1371430710862,140.6,337.955102193633,79.1875,353.3640817549064,68.35C368.7730613161798,57.51249999999999,399.5910204387266,57.51249999999998,415,53.89999999999998" stroke-width="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><circle cx="45.015625" cy="227.3" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="106.65154324509356" cy="68.35" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="168.28746149018713" cy="140.6" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="229.9233797352807" cy="68.35" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="291.7281635098128" cy="140.6" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="353.3640817549064" cy="68.35" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="415" cy="53.89999999999998" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><path fill="#99cac0" stroke="none" d="M45.015625,270.65C60.42460456127339,230.91249999999997,91.24256368382018,122.5375,106.65154324509356,111.69999999999999C122.06052280636695,100.86249999999998,152.87848192891374,183.95,168.28746149018713,183.95C183.6964410514605,183.95,214.5144001740073,111.69999999999999,229.9233797352807,111.69999999999999C245.37457567891371,111.69999999999999,276.2769675661798,183.95,291.7281635098128,183.95C307.1371430710862,183.95,337.955102193633,122.5375,353.3640817549064,111.69999999999999C368.7730613161798,100.86249999999998,399.5910204387266,100.8625,415,97.25L415,314L45.015625,314Z" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></path><path fill="none" stroke="#5fbeaa" d="M45.015625,270.65C60.42460456127339,230.91249999999997,91.24256368382018,122.5375,106.65154324509356,111.69999999999999C122.06052280636695,100.86249999999998,152.87848192891374,183.95,168.28746149018713,183.95C183.6964410514605,183.95,214.5144001740073,111.69999999999999,229.9233797352807,111.69999999999999C245.37457567891371,111.69999999999999,276.2769675661798,183.95,291.7281635098128,183.95C307.1371430710862,183.95,337.955102193633,122.5375,353.3640817549064,111.69999999999999C368.7730613161798,100.86249999999998,399.5910204387266,100.8625,415,97.25" stroke-width="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><circle cx="45.015625" cy="270.65" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="106.65154324509356" cy="111.69999999999999" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="168.28746149018713" cy="183.95" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="229.9233797352807" cy="111.69999999999999" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="291.7281635098128" cy="183.95" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="353.3640817549064" cy="111.69999999999999" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="415" cy="97.25" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><path fill="#707f8e" stroke="none" d="M45.015625,299.55C60.42460456127339,276.06875,91.24256368382018,212.85,106.65154324509356,205.625C122.06052280636695,198.4,152.87848192891374,241.75,168.28746149018713,241.75C183.6964410514605,241.75,214.5144001740073,205.625,229.9233797352807,205.625C245.37457567891371,205.625,276.2769675661798,241.75,291.7281635098128,241.75C307.1371430710862,241.75,337.955102193633,212.85,353.3640817549064,205.625C368.7730613161798,198.4,399.5910204387266,189.36874999999998,415,183.95L415,314L45.015625,314Z" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></path><path fill="none" stroke="#566676" d="M45.015625,299.55C60.42460456127339,276.06875,91.24256368382018,212.85,106.65154324509356,205.625C122.06052280636695,198.4,152.87848192891374,241.75,168.28746149018713,241.75C183.6964410514605,241.75,214.5144001740073,205.625,229.9233797352807,205.625C245.37457567891371,205.625,276.2769675661798,241.75,291.7281635098128,241.75C307.1371430710862,241.75,337.955102193633,212.85,353.3640817549064,205.625C368.7730613161798,198.4,399.5910204387266,189.36874999999998,415,183.95" stroke-width="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><circle cx="45.015625" cy="299.55" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="106.65154324509356" cy="205.625" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="168.28746149018713" cy="241.75" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="229.9233797352807" cy="205.625" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="291.7281635098128" cy="241.75" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="353.3640817549064" cy="205.625" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="415" cy="183.95" r="0" fill="#ffffff" stroke="#999999" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle></svg><div class="morris-hover morris-default-style" style="left: 342.625px; top: 72px; display: none;"><div class="morris-hover-row-label">2015</div><div class="morris-hover-point" style="color: #689bc3">
  Desktops :
  90
</div><div class="morris-hover-point" style="color: #a2b3bf">
  Tablets :
  60
</div><div class="morris-hover-point" style="color: #64b764">
  Mobiles :
  30
</div></div></div>

                                </div>

                            </div>

                            <!-- col -->

                            <div class="col-lg-12 col-xl-6">
                                <div class="card-box">
                                    <a href="#" class="pull-right btn btn-inverse btn-sm waves-effect waves-light">View All</a>
                                    <h4 class="text-dark header-title m-t-0">Recent Orders</h4>
                                    <p class="text-muted m-b-30 font-13">
                                        Use the button classes on an element.
                                    </p>

                                    <div class="table-responsive">
                                        <table class="table table-actions-bar m-b-0">
                                            <thead>
                                            <tr>
                                                <th>Chart</th>
                                                <th>Item Name</th>
                                                <th>Up-Down</th>
                                                <th style="min-width: 80px;">Manage</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><span data-plugin="peity-bar" data-colors="#2bbbad,#2bbbad" data-width="80" data-height="30" style="display: none;">5,3,9,6,5,9,7,3,5,2</span><svg class="peity" height="30" width="80"><rect data-value="5" fill="#2bbbad" x="0.8" y="13.333333333333332" width="6.4" height="16.666666666666668"></rect><rect data-value="3" fill="#2bbbad" x="8.8" y="20" width="6.399999999999999" height="10"></rect><rect data-value="9" fill="#2bbbad" x="16.8" y="0" width="6.399999999999999" height="30"></rect><rect data-value="6" fill="#2bbbad" x="24.8" y="10" width="6.399999999999999" height="20"></rect><rect data-value="5" fill="#2bbbad" x="32.8" y="13.333333333333332" width="6.400000000000006" height="16.666666666666668"></rect><rect data-value="9" fill="#2bbbad" x="40.8" y="0" width="6.400000000000006" height="30"></rect><rect data-value="7" fill="#2bbbad" x="48.8" y="6.666666666666668" width="6.400000000000006" height="23.333333333333332"></rect><rect data-value="3" fill="#2bbbad" x="56.8" y="20" width="6.400000000000006" height="10"></rect><rect data-value="5" fill="#2bbbad" x="64.8" y="13.333333333333332" width="6.400000000000006" height="16.666666666666668"></rect><rect data-value="2" fill="#2bbbad" x="72.8" y="23.333333333333336" width="6.400000000000006" height="6.666666666666664"></rect></svg></td>
                                                <td><img src="assets/images/products/iphone.jpg" class="thumb-sm pull-left m-r-10" alt=""> Apple iPhone </td>
                                                <td><span class="text-success">+$230</span></td>
                                                <td>
                                                    <a href="#" class="table-action-btn"><i class="md md-edit"></i></a>
                                                    <a href="#" class="table-action-btn"><i class="md md-close"></i></a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><span data-plugin="peity-line" data-fill="#33b5e5" data-stroke="#33b5e5" data-width="80" data-height="30" style="display: none;">0,3,6,4,5,4,7,3,5,2</span><svg class="peity" height="30" width="80"><polygon fill="#33b5e5" points="0 29.5 0 29.5 8.88888888888889 17.071428571428573 17.77777777777778 4.642857142857146 26.666666666666668 12.92857142857143 35.55555555555556 8.785714285714285 44.44444444444444 12.92857142857143 53.333333333333336 0.5 62.22222222222223 17.071428571428573 71.11111111111111 8.785714285714285 80 21.214285714285715 80 29.5"></polygon><polyline fill="none" points="0 29.5 8.88888888888889 17.071428571428573 17.77777777777778 4.642857142857146 26.666666666666668 12.92857142857143 35.55555555555556 8.785714285714285 44.44444444444444 12.92857142857143 53.333333333333336 0.5 62.22222222222223 17.071428571428573 71.11111111111111 8.785714285714285 80 21.214285714285715" stroke="#33b5e5" stroke-width="1" stroke-linecap="square"></polyline></svg></td>
                                                <td><img src="assets/images/products/samsung.jpg" class="thumb-sm pull-left m-r-10" alt=""> Samsung Phone </td>
                                                <td><span class="text-danger">-$154</span></td>
                                                <td>
                                                    <a href="#" class="table-action-btn"><i class="md md-edit"></i></a>
                                                    <a href="#" class="table-action-btn"><i class="md md-close"></i></a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><span data-plugin="peity-line" data-fill="#fff" data-stroke="#ff3547" data-width="80" data-height="30" style="display: none;">5,3,9,6,5,9,7,3,5,2</span><svg class="peity" height="30" width="80"><polygon fill="#fff" points="0 29.5 0 13.38888888888889 8.88888888888889 19.833333333333336 17.77777777777778 0.5 26.666666666666668 10.166666666666668 35.55555555555556 13.38888888888889 44.44444444444444 0.5 53.333333333333336 6.944444444444443 62.22222222222223 19.833333333333336 71.11111111111111 13.38888888888889 80 23.055555555555557 80 29.5"></polygon><polyline fill="none" points="0 13.38888888888889 8.88888888888889 19.833333333333336 17.77777777777778 0.5 26.666666666666668 10.166666666666668 35.55555555555556 13.38888888888889 44.44444444444444 0.5 53.333333333333336 6.944444444444443 62.22222222222223 19.833333333333336 71.11111111111111 13.38888888888889 80 23.055555555555557" stroke="#ff3547" stroke-width="1" stroke-linecap="square"></polyline></svg>
                                                </td><td><img src="assets/images/products/imac.jpg" class="thumb-sm pull-left m-r-10" alt=""> Apple iPhone </td>
                                                <td><span class="text-success">+$1850</span></td>
                                                <td>
                                                    <a href="#" class="table-action-btn"><i class="md md-edit"></i></a>
                                                    <a href="#" class="table-action-btn"><i class="md md-close"></i></a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><span data-plugin="peity-pie" data-colors="#4c5667,#ebeff2" data-width="30" data-height="30" style="display: none;">1/5</span><svg class="peity" height="30" width="30"><path d="M 15.000000000000002 0 A 15 15 0 0 1 29.265847744427305 10.36474508437579 L 15 15" data-value="1" fill="#4c5667"></path><path d="M 29.265847744427305 10.36474508437579 A 15 15 0 1 1 14.999999999999996 0 L 15 15" data-value="4" fill="#ebeff2"></path></svg></td>
                                                <td><img src="assets/images/products/macbook.jpg" class="thumb-sm pull-left m-r-10" alt=""> Apple iPhone </td>
                                                <td><span class="text-success">+$560</span></td>
                                                <td>
                                                    <a href="#" class="table-action-btn"><i class="md md-edit"></i></a>
                                                    <a href="#" class="table-action-btn"><i class="md md-close"></i></a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><span data-plugin="peity-bar" data-colors="#7266ba,#ebeff2" data-width="80" data-height="30" style="display: none;">5,3,9,6,5,9,7,3,5,2</span><svg class="peity" height="30" width="80"><rect data-value="5" fill="#7266ba" x="0.8" y="13.333333333333332" width="6.4" height="16.666666666666668"></rect><rect data-value="3" fill="#ebeff2" x="8.8" y="20" width="6.399999999999999" height="10"></rect><rect data-value="9" fill="#7266ba" x="16.8" y="0" width="6.399999999999999" height="30"></rect><rect data-value="6" fill="#ebeff2" x="24.8" y="10" width="6.399999999999999" height="20"></rect><rect data-value="5" fill="#7266ba" x="32.8" y="13.333333333333332" width="6.400000000000006" height="16.666666666666668"></rect><rect data-value="9" fill="#ebeff2" x="40.8" y="0" width="6.400000000000006" height="30"></rect><rect data-value="7" fill="#7266ba" x="48.8" y="6.666666666666668" width="6.400000000000006" height="23.333333333333332"></rect><rect data-value="3" fill="#ebeff2" x="56.8" y="20" width="6.400000000000006" height="10"></rect><rect data-value="5" fill="#7266ba" x="64.8" y="13.333333333333332" width="6.400000000000006" height="16.666666666666668"></rect><rect data-value="2" fill="#ebeff2" x="72.8" y="23.333333333333336" width="6.400000000000006" height="6.666666666666664"></rect></svg></td>
                                                <td><img src="assets/images/products/lumia.jpg" class="thumb-sm pull-left m-r-10" alt=""> Lumia iPhone </td>
                                                <td><span class="text-success">+$230</span></td>
                                                <td>
                                                    <a href="#" class="table-action-btn"><i class="md md-edit"></i></a>
                                                    <a href="#" class="table-action-btn"><i class="md md-close"></i></a>
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                            <!-- end col -->

                        </div>
                        <!-- end row -->


                    </div> <!-- container -->

                </div>
             <!-- content -->

             <!-- Footer Start -->
             <?php  $this->load->view('incl/footer'); ?>
             <!-- Footer End -->
         </div>
         
     </div>

     <!-- jQuery  & Javacript -->
     <?php $this->load->view('incl/script'); ?>
 
     <!-- Javascript Internal -->
 
 </body>
</html>