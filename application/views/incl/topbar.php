<div class="topbar">

<!-- LOGO -->
<div class="topbar-left">
    <div class="text-center">
        <a> <img src="<?= base_url('') ?>template/assets/images/logo.jpeg" width="70" height="70"></a>
    </div>
</div>

<!-- Button mobile view to collapse sidebar menu -->

<nav class="navbar-custom">
     <ul class="list-inline menu-left mb-0"><a class="logo"><span>Smart Sales Analyst System</span></a>
        <li class="float-left">
            <button class="button-menu-mobile open-left waves-light waves-effect">
            </button>                        
        </li>
    </ul>
</nav>

</div>