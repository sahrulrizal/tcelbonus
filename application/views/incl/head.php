<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        <link rel="shortcut icon" href="<?= base_url('') ?>template/assets/images/logo.jpeg">
        <title>Telkomcels - Smart Sales Analyst Systems</title>
        <link href="<?= base_url('') ?>template/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('') ?>template/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('') ?>template/assets/css/style.css" rel="stylesheet" type="text/css" />
<!-- google maps api -->
        <script src="<?= base_url('') ?>template/assets/js/modernizr.min.js"></script>

      

  
    </head>