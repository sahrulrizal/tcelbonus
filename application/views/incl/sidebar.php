<div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>

                            <li class="text-muted menu-title">Navigation</li>

                            <li class="has_sub">
                                <a href="<?=site_url('');?>" class="waves-effect"><i class="ti-home"></i> <span> Home </span> <span class="menu-arrow"></span></a>
                            </li>

                            <li class="has_sub">
                                <a href="<?=site_url('');?>" class="waves-effect"><i class="ti-view-list-alt"></i> <span> Device </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="ui-loading-buttons.php">Profile Subscribes</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="<?=site_url('welcome/maps');?>" class="waves-effect"><i class="ti-location-pin"></i><span> BTS Maps </span> <span class="menu-arrow"></span></a>
                            </li>

                            <!-- <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="ti-paint-bucket"></i> <span> UI Kit </span> <span class="menu-arrow"></span> </a>
                                <ul class="list-unstyled">
                                    <li><a href="ui-buttons.php">Buttons</a></li>
                                    <li><a href="ui-loading-buttons.php">Loading Buttons</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-spray"></i> <span> Icons </span> <span class="menu-arrow"></span> </a>
                                <ul class="list-unstyled">
                                    <li><a href="icons-materialdesign.php">Material Design</a></li>
                                    <li><a href="icons-ionicons.php">Ion Icons</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-pencil-alt"></i><span> Forms </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="form-elements.php">General Elements</a></li>
                                    <li><a href="form-advanced.php">Advanced Form</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-menu-alt"></i><span>Tables </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="tables-basic.php">Basic Tables</a></li>
                                    <li><a href="tables-datatable.php">Data Table</a></li>
                                </ul>
                            </li> -->

                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>