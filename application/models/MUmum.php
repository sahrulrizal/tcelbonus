<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MUmum extends CI_Model {

	public function locations($param=null,$idb=null)
	{
		if ($param == "select") {
			$id = $idb;
		}else{
			$id = $this->input->get('id');
		}
		
		if ($id != '') {
			$q = $this->db->get_where('locations l',
				array(
					'l.idlocation' => $id
				)
			);

			// Deklarasi
			$count = $q->num_rows();
			$result = $q->result();

			if ($count > 0) {

				if ($count > 1) {
					$data = array(
						'result' => $result,
						'msg' => "Data is more than 1",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}else{
					$data = array(
						'result' => $result,
						'msg' => "Data is Ready",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}
				
			}else{
				$data = array(
					'result' => $q->row(),
					'msg' => "Data does not exist",
					'status' => false,
					'info' => 'error',
					'count' =>  $count
				);
			}

		}else{
			
			$q = $this->db->get_where('locations l');

			// Deklarasi
			$count = $q->num_rows();
			$result = $q->result();

			if ($count > 0) {

				if ($count > 1) {
					$data = array(
						'result' => $result,
						'msg' => "Data is more than 1",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}else{
					$data = array(
						'result' => $result,
						'msg' => "Data does is exist",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}
				
			}else{
				$data = array(
					'result' => $q->row(),
					'msg' => "Data does not exist",
					'status' => false,
					'info' => 'error',
					'count' =>  $count
				);
			}
		}

		return json_encode($data);
	}

	public function config($param=null,$idb=null)
	{
		if ($param == "select") {
			$id = $idb;
		}else{
			$id = $this->input->get('id');
		}
		
		if ($id != '') {
			$q = $this->db->get_where('config c',
				array(
					'c.idconfig' => $id
				)
			);

			// Deklarasi
			$count = $q->num_rows();
			$result = $q->result();

			if ($count > 0) {

				if ($count > 1) {
					$data = array(
						'result' => $result,
						'msg' => "Data is more than 1",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}else{
					$data = array(
						'result' => $result,
						'msg' => "Data is Ready",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}
				
			}else{
				$data = array(
					'result' => $q->row(),
					'msg' => "Data does not exist",
					'status' => false,
					'info' => 'error',
					'count' =>  $count
				);
			}

		}else{
			
			$q = $this->db->get_where('config c');

			// Deklarasi
			$count = $q->num_rows();
			$result = $q->result();

			if ($count > 0) {

				if ($count > 1) {
					$data = array(
						'result' => $result,
						'msg' => "Data is more than 1",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}else{
					$data = array(
						'result' => $result,
						'msg' => "Data does is exist",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}
				
			}else{
				$data = array(
					'result' => $q->row(),
					'msg' => "Data does not exist",
					'status' => false,
					'info' => 'error',
					'count' =>  $count
				);
			}
		}

		return json_encode($data);
	}

}