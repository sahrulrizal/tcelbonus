<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MLogic extends CI_Model {
	
	public function convertToBytes($quota)
	{
		$units  = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
        $number = substr($quota, 0, -2);
        $suffix = strtoupper(substr($quota, -2));

        //B or no suffix
        if (is_numeric(substr($suffix, 0, 1))) {
            return preg_replace('/[^\d]/', '', $quota);
        }

        $exponent = array_flip($units)[$suffix] ?? null;
        if ($exponent === null) {
            return null;
        }

        return $number * (1024 ** $exponent);
	}

	function generateRandomInt($length = 7) {
        $characters       = '0123456789';
        $charactersLength = strlen($characters);
        $randomString     = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
	
}
