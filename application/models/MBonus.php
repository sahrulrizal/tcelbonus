<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MBonus extends CI_Model {
	
	public function bonus($param='',$idb='')
	{

		if ($param == "select") {
			$id = $idb;
		}else{
			$id = $this->input->get('id');
		}
		
		if ($id != '') {
			$this->db->select('u.name as name_user,b.*');
			$this->db->join('users u', 'u.iduser = b.by', 'inner');
			$q = $this->db->get_where('bonus b',
				array(
					'b.idbonus' => $id,
					'u.status' => '1'
				)
			);

			// Deklarasi
			$count = $q->num_rows();
			$result = $q->result();

			if ($count > 0) {

				if ($count > 1) {
					$data = array(
						'result' => $result,
						'msg' => "Data is more than 1",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}else{
					$data = array(
						'result' => $result,
						'msg' => "Data is Ready",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}
				
			}else{
				$data = array(
					'result' => $q->row(),
					'msg' => "Data does not exist",
					'status' => false,
					'info' => 'error',
					'count' =>  $count
				);
			}

		}else{
			
			$this->db->select('u.name as name_user,b.*');
			$this->db->join('users u', 'u.iduser = b.by', 'inner');
			$q = $this->db->get_where('bonus b',
				array(
					'u.status' => '1'
				)
			);

			// Deklarasi
			$count = $q->num_rows();
			$result = $q->result();

			if ($count > 0) {

				if ($count > 1) {
					$data = array(
						'result' => $result,
						'msg' => "Data is more than 1",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}else{
					$data = array(
						'result' => $result,
						'msg' => "Data does is exist",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}
				
			}else{
				$data = array(
					'result' => $q->row(),
					'msg' => "Data does not exist",
					'status' => false,
					'info' => 'error',
					'count' =>  $count
				);
			}
		}

		return json_encode($data);
	}

	public function sendBonusToMsisdn()
	{
		
		// GET OTHER MODEL
		$CI =& get_instance();
        $CI->load->model('MLog','ml');
        $CI->load->model('MCharging','mc');

		//Deklarasi variabel
		$msisdn = $this->input->post('msisdn');
		$id = $this->input->post('id');
		$bonusnya = ''; 

		// Query untuk cek bonus apakah sudah dapat bonus hari ini
		$cekBonus = $this->db->query("SELECT * FROM listGetBonus where msisdn = '".$msisdn."' AND DATE(date) = '".date('Y-m-d')."'");
		
		// Pengkondisian
		if ($msisdn == '') {
			$data = array(
				'result' => null,
				'msg' => "Sorry your msisdn can't be null",
				'status' => false,
				'info' => 'error',
				'count' => 1
			);
		}else if ($cekBonus->num_rows() > 0) {
			$data = array(
				'result' => null,
				'msg' => "You just have received your bonus today!",
				'status' => false,
				'info' => 'error',
				'count' => 1
			);
		}else{
			
			// Get Bonus
			$b = json_decode($this->mb->bonus('select',1));
			$bonus = $b->result[0];

			// Jika pengiriman sukses ke charging
			$sendBPD = $CI->mc->sendBonusPackageData($msisdn,$bonus->quota,$bonus->validity);
			$rsendBPD = json_decode(json_encode($sendBPD));

			if ($rsendBPD->result == 0 || $rsendBPD->result == '0') {
			 	
			 	// Insert to logTransaksi
				$in = $CI->ml->logTransaction('insert', array(
					'by' => $id,
					'destination' => $msisdn,
					'msg' => 'Sending bonus '.$bonusnya.' to '.$msisdn.' is Success',
					'response' => $rsendBPD->all,
					'date' => date('Y-m-d H:i:s'),
					'status' => 1
				));

				// Insert To List Bonus
				$in2 = $this->listGetBonus('insert',array(
					'msisdn' => $msisdn,
					'by' => $id,
					'msg' => 'Send Bonus To '.$msisdn,
					'date' => date('Y-m-d H:i:s'),
					'idbonus' => $bonus->idbonus
				));

				// Insert to logAction
				$in3 = $CI->ml->logAction('insert', array(
					'msg' => 'Melakukan pengiriman Bonus Ke Nomor '.$msisdn,
					'by' => $id,
					'ip' => $this->input->ip_address(),
					'info' => 2, // 1 => Transaction
					'date' => date('Y-m-d H:i:s'),
					'status' => 1
				));


				// Replace sms
				$msg = str_replace('~quotaName', $bonus->quotaName, $bonus->msg);
				
				// Kirim SMS
				$sms = $CI->mc->sendSms($msisdn,$msg);

				// Membuat Array
				$arrInsert = array($in,$in2,$in3,$sms,$sendBPD);

				// Buat Pesan Response
				$pesan = " Sending Bonus to ".$msisdn.' is Success';
				$status = true;

			}else{
				// Insert to logTransaksi
				$in = $CI->ml->logTransaction('insert', array(
					'by' => $id,
					'destination' => $msisdn,
					'msg' => 'Sending bonus '.$bonusnya.' to '.$msisdn.' is Failed',
					'response' => $rsendBPD->all,
					'date' => date('Y-m-d H:i:s'),
					'status' => 0
				));

				// Insert to logAction
				$in3 = $CI->ml->logAction('insert', array(
					'msg' => 'Melakukan pengiriman Bonus Ke Nomor '.$msisdn.' Gagal',
					'by' => $id,
					'ip' => $this->input->ip_address(),
					'info' => 2, // 1 => Transaction
					'date' => date('Y-m-d H:i:s'),
					'status' => 0
				));

				// Buat array
				$arrInsert = array($in,$in3,$sendBPD);

				// Buat Pesan Response
				$pesan = " Sending Bonus to ".$msisdn.' is Failed';
				$status = false;
			} 

			// Response
			$data = array(
				'result' => true,
				'msg' => $pesan,
				'status' => $status,
				'other' => $arrInsert, 
				'info' => 'good',
			);


		}

		echo json_encode($data);
	}

	public function listGetBonus($aksi,$arr)
	{
		// NAMA TABEL
		$table = 'listGetBonus';
		$dek = 'lgb';

		if ($aksi == "insert") {
			$q = $this->db->insert($table, $arr);
			
			if ($q) {
				$data = array(
						'result' => $q,
						'msg' => "Insert is success",
						'status' => true,
						'name' =>$table,
						'info' => 'good'
					);

			}else{
				$data = array(
						'result' => $q,
						'msg' => "Insert is failed",
						'status' => true,
						'name' =>$table,
						'info' => 'error'
					);
			}
			
		}else{

			$id = $this->input->get('id');

			if($id != ''){

				$this->db->select('u.iduser,u.username,lgb.*');
				$this->db->join('users u', 'u.iduser = lgb.by', 'inner');
				$q = $this->db->get_where($table.' '.$dek, array('idlistGetBonus' => $id));

			}else{

			    $this->db->select('u.iduser,u.username,lgb.*');
				$this->db->join('users u', 'u.iduser = lgb.by', 'inner');
				$q = $this->db->get($table.' '.$dek);

			}

			// Deklarasi
			$count = $q->num_rows();
			$result = $q->result();

			if ($count > 0) {

				if ($count > 1) {
					$data = array(
						'result' => $result,
						'msg' => "Data is more than 1",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}else{
					$data = array(
						'result' => $result,
						'msg' => "Data does is exist",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}
				
			}else{

				$data = array(
					'result' => $q->row(),
					'msg' => "Data does not exist",
					'status' => false,
					'info' => 'error',
					'count' =>  $count
				);
			}
		}

		return $data;
	}
	
}
