<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MCharging extends CI_Model {

    public function getToken() {
        
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_PORT           => "8280",
            CURLOPT_URL            => "http://150.242.110.240:8280/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_HTTPHEADER     => array(
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            $r = json_decode($response);
            return $r->access_token;
        }
    }

	public function sendSms($msisdn,$msg) {

        $sender = "TCELBONUS";

        $ch     = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $msisdn = trim($msisdn);
        if (substr($msisdn, 0, 3) != '670') {//add for prevent miss typing or else
            $msisdn = '670' . $msisdn;
        }

        curl_setopt($ch, CURLOPT_URL, 'http://172.17.12.63:8280/sendSMS?msg=' . urlencode($msg) . '&sender=' . $sender . '&to=' . $msisdn . '');
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CAINFO, NULL);
        curl_setopt($ch, CURLOPT_CAPATH, NULL);

        $content = curl_exec($ch);

        if (curl_error($ch)) {
            $error_msg = curl_error($ch);
        }

        curl_close($ch);

        $status = true;
        $data = array(
        	'result' => $content,
        	'msg' => $msg,
        	'name' => 'Send Sms',
        	'date' => date('Y-m-d H:i:s'),
        	'status' => $status
        );

        return $data;
    }

     public function sendBonusPackageData($msisdn=null,$quota=null,$validity=null) 
     {

        // GET OTHER MODEL
        $CI =& get_instance();
        $CI->load->model('MLogic','logic');

        //Delkarasi
        $trxid = $CI->logic->generateRandomInt();
        $Quota = $CI->logic->convertToBytes($quota);

        $curl  = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT           => "8280",
            CURLOPT_URL            => "http://172.17.12.63:8280/telkomcel/bta?msisdn=" . $msisdn . "&trxid=" . $trxid . "&originTransactionID=" . $trxid . "&appId=125&externalData1=TelkomcelBonusPertamina&expireDate=" . $validity . "&dedicatedAccountID=6&adjustmentAmountRelative=" . $Quota . "&type=16&dedicatedAccountUnitType=6",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_HTTPHEADER     => array(
                "authorization: Bearer " . $this->getToken(),
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        if ($err) {
            
            $data = array(
                'result' => null,
                'all' => "cURL Error #:" . $err,
                'status' => false
            );

        } else {
            $r = explode(' ',$response);
            
            $data = array(
                'destination' => $r[0],
                'trx_code' => $r[1],
                'result' => $r[2],
                'all' => $response, 
                'status' => true
            );

        }

        return $data;
    }

}