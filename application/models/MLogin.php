<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MLogin extends CI_Model {

	public function login()
	{	
		// GET OTHER MODEL
		$CI =& get_instance();
        $CI->load->model('MLog','ml');

		//Deklarasi variabel
		$username = $this->input->post('username'); 
		$password = $this->input->post('password'); 
		$location = $this->input->post('location'); 
		
		// Pengkondisian
		if ($username == '') {
			$data = array(
				'result' => null,
				'msg' => "Sorry your username can't be null",
				'status' => false,
				'info' => 'error',
				'count' => 1
			);
		}else if (!preg_match('/^[a-z\d_]{3,20}$/i', $username)) {
			$data = array(
				'result' => null,
				'msg' => "Username must be at least 3 characters and Space, Special Character are not allowed",
				'status' => false,
				'info' => 'error',
				'count' => 1
			);
		}else if ($password == '') {
			$data = array(
				'result' => null,
				'msg' => "Sorry your password can't be null",
				'status' => false,
				'info' => 'error',
				'count' => 1
			);
		}else if ($location == '') {
			$data = array(
				'result' => null,
				'msg' => "Sorry your location can't be null",
				'status' => false,
				'info' => 'error',
				'count' => 1
			);
		}else{

			$this->db->select('u.iduser,username,u.name,l.name as location,lat,lng,u.status');
			$this->db->join('locations l ', 'l.idlocation = u.location', 'inner');
			$q = $this->db->get_where('users u',
				array(
					'u.location' => $location,
					'username' => $username,
					'password' => md5($password),
					'verify_password' => $password,
					'u.status' => '1'
				)
			);

			// Deklarasi
			$count = $q->num_rows();
			$result = $q->result();

			if ($count > 0) {

				if ($count > 1) {
					$data = array(
						'result' => $result,
						'msg' => "Data is more than 1",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}else{

					// Insert to logAction
					$in = $CI->ml->logAction('insert', array(
						'msg' => 'Login ke Aplikasi',
						'by' => $q->row()->iduser,
						'ip' => $this->input->ip_address(),
						'info' => 1, // 1 => Login
						'date' => date('Y-m-d H:i:s'),
						'status' => 1
					));

					$data = array(
						'result' => $result,
						'msg' => "Login successfully",
						'status' => true,
						'info' => 'good',
						'count' =>  $count,
						'other' => $in
					);


				}
				
			}else{

				$data = array(
					'result' => $q->row(),
					'msg' => "Can't login, please check username and password!",
					'status' => false,
					'info' => 'error',
					'count' =>  $count
				);
			}
			

		}

		echo json_encode($data);
		
	}

	public function logout()
	{	
		// GET OTHER MODEL
		$CI =& get_instance();
        $CI->load->model('MLog','ml');

		// Insert to logAction
		$in = $CI->ml->logAction('insert', array(
			'msg' => 'Logout dari Aplikasi',
			'by' => $this->input->post('id'),
			'info' => 3, // 1 => Login
			'date' => date('Y-m-d H:i:s'),
			'status' => 1,
			'ip' => $this->input->ip_address()
		));

		$data = array(
			'result' => 'Logout Sukses',
			'msg' => "Logout is Success",
			'status' => true,
			'info' => 'good',
			'other' => $in
		);

		echo json_encode($data);
	}

	public function users()
	{
		$id = $this->input->get('id');
		
		if ($id != '') {
			$this->db->select('u.iduser,username,p.name as position,u.name,l.name as location,lat,lng,u.status');
			$this->db->join('locations l', 'l.idlocation = u.location', 'inner');
			$this->db->join('positions p', 'p.idpositions = u.position', 'inner');
			$q = $this->db->get_where('users u',
				array(
					'u.iduser' => $id,
					'u.status' => '1'
				)
			);

			// Deklarasi
			$count = $q->num_rows();
			$result = $q->result();

			if ($count > 0) {

				if ($count > 1) {
					$data = array(
						'result' => $result,
						'msg' => "Data is more than 1",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}else{
					$data = array(
						'result' => $result,
						'msg' => "Login successfully",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}
				
			}else{
				$data = array(
					'result' => $q->row(),
					'msg' => "Data does not exist",
					'status' => false,
					'info' => 'error',
					'count' =>  $count
				);
			}

		}else{
			$this->db->select('u.iduser,username,p.name as position,u.name,l.name as location,lat,lng,u.status');
			$this->db->join('locations l', 'l.idlocation = u.location', 'inner');
			$this->db->join('positions p', 'p.idpositions = u.position', 'inner');
			$q = $this->db->get('users u');

			// Deklarasi
			$count = $q->num_rows();
			$result = $q->result();

			if ($count > 0) {

				if ($count > 1) {
					$data = array(
						'result' => $result,
						'msg' => "Data is more than 1",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}else{
					$data = array(
						'result' => $result,
						'msg' => "Data does is exist",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}
				
			}else{
				$data = array(
					'result' => $q->row(),
					'msg' => "Data does not exist",
					'status' => false,
					'info' => 'error',
					'count' =>  $count
				);
			}
		}

		echo json_encode($data);
	}
}
