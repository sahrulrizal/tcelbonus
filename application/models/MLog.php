<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MLog extends CI_Model {
	
	// Log Transacrton

	public function logTransaction($aksi=null,$arr=null,$where=null)
	{
		// NAMA TABEL
		$table = 'logTransaction';
		$dek = 'lt';

		if ($aksi == "insert") {
			$q = $this->db->insert($table, $arr);
			
			if ($q) {
				$data = array(
						'result' => $q,
						'msg' => "Insert is success",
						'data' => $arr,
						'id' => $this->db->insert_id(),
						'date' => date('Y-m-d H:i:s'),
						'status' => true,
						'info' => 'good'
					);

			}else{
				$data = array(
						'result' => $q,
						'data' => $arr,
						'id' => '',
						'msg' => "Insert is failed",
						'status' => true,
						'info' => 'error'
					);
			}
			
		}else if ($aksi == "update") {
			$q = $this->db->update($table, $arr, $where);
			
			if ($q) {
				$data = array(
						'result' => $q,
						'msg' => "Update is success",
						'data' => $arr,
						'date' => date('Y-m-d H:i:s'),
						'status' => true,
						'info' => 'good'
					);

			}else{
				$data = array(
						'result' => $q,
						'data' => $arr,
						'date' => date('Y-m-d H:i:s'),
						'msg' => "Update is failed",
						'status' => true,
						'info' => 'error'
					);
			}
			
		}else{

			$id = $this->input->get('id');
			$idWhere = $this->input->get('idWhere');

			if($id != ''){
				$this->db->select('u.iduser,u.username,lt.*');
				$this->db->join('users u', 'u.iduser = lt.by', 'inner');
				$q = $this->db->get_where($table.' '.$dek, array('idlogtrans' => $id));
			}elseif($idWhere != ''){
				$this->db->order_by('lt.idlogtrans', 'desc');
				$this->db->select('u.iduser,u.username,lt.*');
				$this->db->limit(10);
				$this->db->join('users u', 'u.iduser = lt.by', 'inner');
				$q = $this->db->get_where($table.' '.$dek, $arr);
			}else{
			   
			    $this->db->select('u.iduser,u.username,lt.*');
				$this->db->join('users u', 'u.iduser = lt.by', 'inner');
				$q = $this->db->get($table.' '.$dek);

			}

			// Deklarasi
			$count = $q->num_rows();
			$result = $q->result();

			if ($count > 0) {

				if ($count > 1) {
					$data = array(
						'result' => $result,
						'msg' => "Data is more than 1",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}else{
					$data = array(
						'result' => $result,
						'msg' => "Data does is exist",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}
				
			}else{

				$data = array(
					'result' => $q->row(),
					'msg' => "Data does not exist",
					'status' => false,
					'info' => 'error',
					'count' =>  $count
				);
			}
		}

		return $data;
	}

	// LOG ACTION

	public function logAction($aksi,$arr)
	{
		// NAMA TABEL
		$table = 'logAction';
		$dek = 'la';

		if ($aksi == "insert") {
			$q = $this->db->insert($table, $arr);
			
			if ($q) {
				$data = array(
						'result' => $q,
						'msg' => "Insert is success",
						'status' => true,
						'info' => 'good'
					);

			}else{
				$data = array(
						'result' => $q,
						'msg' => "Insert is failed",
						'status' => true,
						'info' => 'error'
					);
			}
			
		}else{

			$id = $this->input->get('id');

			if($id != ''){

				$this->db->select('u.iduser,u.username,la.*');
				$this->db->join('users u', 'u.iduser = la.by', 'inner');
				$q = $this->db->get_where($table.' '.$dek, array('idlogact' => $id));

			}else{

			    $this->db->select('u.iduser,u.username,la.*');
				$this->db->join('users u', 'u.iduser = la.by', 'inner');
				$q = $this->db->get($table.' '.$dek);

			}

			// Deklarasi
			$count = $q->num_rows();
			$result = $q->result();

			if ($count > 0) {

				if ($count > 1) {
					$data = array(
						'result' => $result,
						'msg' => "Data is more than 1",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}else{
					$data = array(
						'result' => $result,
						'msg' => "Data does is exist",
						'status' => true,
						'info' => 'good',
						'count' =>  $count
					);
				}
				
			}else{

				$data = array(
					'result' => $q->row(),
					'msg' => "Data does not exist",
					'status' => false,
					'info' => 'error',
					'count' =>  $count
				);
			}
		}

		return $data;
	}

	public function toLogAction()
	{
		$in = $this->logAction('insert', array(
			'msg' => $this->input->post('msg'),
			'by' => $this->input->post('id'),
			'info' => 4, // 1 => Login
			'date' => date('Y-m-d H:i:s'),
			'status' => true,
			'ip' => $this->input->ip_address()
		));

		$data = array(
			'result' => '',
			'msg' => "Saving page",
			'status' => true,
			'info' => 'good',
			'other' => $in
		);

		return $data;
	}
	
}
