<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		header('Access-Control-Allow-Origin: *');

		$this->load->model('MUmum','mu');
		$config = json_decode($this->mu->config('select',1));
		$active = $config->result[0]->active;

		if ($active == 0) {
			redirect('heandle/error_404');
		}else{  
			$this->load->model('MLogin','l');
			$this->load->model('MLog','ml');
			$this->load->model('MBonus','mb');
			$this->load->model('MCharging','mc');
		}
	}

	public function index()
	{

		echo "Hello....";
	}

	public function locations()
	{
		echo $this->mu->locations();
	}

	public function login()
	{
		echo $this->l->login();
	}

	public function logout()
	{
		echo $this->l->logout();
	}

	public function users()
	{
		echo $this->l->users();
	}

	public function toLogAction()
	{
		echo $this->ml->toLogAction();
	}

	public function sendBonusToMsisdn()
	{
		echo $this->mb->sendBonusToMsisdn();
	}

	public function bonus()
	{
		echo $this->mb->bonus();
	}


	public function inUserAll()
	{
		// $file    = file_get_contents('./data/users-1.csv');
		// $file2 = explode('|', $file);
		// foreach ($file2 as $key => $v) {
		// 	$nilai = explode(',', $v);

		// 	$in = $this->db->insert('users', array(
		// 		'name' => $nilai[0],
		// 		'position' => $nilai[1],
		// 		'password' => md5('tcelbonus'),
		// 		'verify_password' => 'tcelbonus', 
		// 		'username' => $nilai[2],
		// 		'msisdn' => $nilai[2],
		// 		'location' => $nilai[3],
		// 		'level' => 1,
		// 		'status' => 1,
		// 		'date' => date('Y-m-d H:i:s')
		// 	));

		// 	echo $in.'<br>';
		// }
	}

	public function getLogTransaction()
	{
		echo json_encode($this->ml->logTransaction('select',array(
			'lt.by' => $this->input->get('idWhere')
		)));
	}

	public function saveHistory()
	{	
		$id = $this->input->post('id');
		$imgContent = file_get_contents($this->input->post('img'));
		$nmaImg = md5($id.'_'.'Telkomcel-bonus').'.jpg';
		$namaImage = './data/struk/'.$nmaImg;

		$img = file_put_contents($namaImage, $imgContent);

		$arr = $this->ml->logTransaction('update', array('img' =>  $nmaImg), array('idlogtrans' => $id));

		if (file_exists($namaImage)) {
			$data = array(
				'data' => $arr,
				'date' => date('Y-m-d H:i:s'),
				'msg' => "Save Receipt is Success",
				'status' => true,
				'info' => 'success'
			);
		}else{
			$data = array(
				'data' => $arr,
				'date' => date('Y-m-d H:i:s'),
				'msg' => "Save Receipt is Failed",
				'status' => false,
				'info' => 'error'
			);
		}

		echo json_encode($data);
	}
}
