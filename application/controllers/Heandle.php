<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Heandle extends CI_Controller {


	function __construct()
	{
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
	}

	public function error_404()
	{
		$data = array(
			'info' => 0,
			'msg' => 'Sorry the application is not activated',
			'status' => false
		);

		echo json_encode($data);
	}

}